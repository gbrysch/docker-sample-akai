Setup Environment
- Activemq 5.11
  - start with activemq/bin/<win32 or win64>/activemq.bat
- Tomcat 7
  - edit web.xml or overwrite it in tomcat/conf/web.xml
  - start with bin/startup.bat
- Java 8
- Maven 3.3.1 

Possible Structure
DEVEVLOP
 - tomcat  
    - tomcat7
 - activemq 
    - activemq5.11
 - java  
    - java7 oder 8
 - maven
    - 3.3.1

Sampleproject dockerSample
- Get Sample aus GIT (pending, Gabi will upload it and send you the repository url)
- clone the sample code to a folder, p.e. C:\AKA\dockerSample>
- Build it from Commandline commandline: C:\AKA\dockerSample>mvn clean install -U -X
- edit web.xml of tomcat to register the sample servlet c:\DEVELOP\tomcat7\conf\web.xml for servlet ProducerServlet
- starting tomcat7
- starting activemq
- See admin console of activemq if you want to http://localhost:8161/admin
- calling servlet that sends a test message  http://localhost:8080/dockerSample/producer
- start client that subscribes to the testqueue/testtopic C:\AKA>java -jar activemq-topic-consumer.jar local-gb tester tcp://localhost:61616 docker.test.topic and prints it to console (stdout)

Container 1: activemq, opens port 61616
Container 2: tomcat mit webapp, dass servlet enthält, das testnachricht erzeugt mit angepasster web.xml
Containner 3: subskribiert Nachrichten in Container1, die aus Container2 produziert wurden !



