package de.dockerSample;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.util.ByteArrayOutputStream;

/**
 * Sample Servlet to send a testmessage to an activemq server.
 * Currentely assumption the activemq server is running on localhost, port 61616
 * @author Bryschg
 *
 */
public class ProducerServlet extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response) {

		try {
			PrintWriter out = response.getWriter();
			String activemqUrl = "tcp://localhost:61616";
			String queueName = null; // "docker.test.queue";
			String topicName = "docker.test.topic";
			HashMap<String, String> properties = new HashMap<>();

			out.println("connecting to ActiveMQ");
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
					activemqUrl);

			connectionFactory.setClientID("testClientId2");
			Connection connection = connectionFactory.createConnection();
			connection.start();
			out.println("connection established");

			out.println("creating session...");
			Session session = connection.createSession(false,
					Session.AUTO_ACKNOWLEDGE);
			out.println("session created");

			final Destination destination;
			if (queueName != null) {
				out.println("creating queue " + queueName);
				destination = session.createQueue(queueName);
				out.println("queue " + queueName + " created");
			} else {
				out.println("creating topic " + topicName);
				destination = session.createTopic(topicName);
				out.println("topic " + topicName + " created");
			}
			out.println("creating producer");
			MessageProducer producer = session.createProducer(destination);
			out.println("producer created");

			TextMessage message = session.createTextMessage();
			for (Entry<String, String> entry : properties.entrySet()) {
				message.setStringProperty(entry.getKey(), entry.getValue());
			}

//			out.println("reading message from stdin:");
//			ByteArrayOutputStream baos = new ByteArrayOutputStream();
//			int read;
//			byte[] buffer = new byte[4096];
//			while ((read = System.in.read(buffer)) != -1) {
//				baos.write(buffer, 0, read);
//			}
//			String text = new String(baos.toByteArray(), StandardCharsets.UTF_8);

			message.setText("<message>hello Daniel !</message>");
			out.println("sending message");
			producer.send(message);
			out.println("message sent (message-id: "
					+ message.getJMSMessageID() + ")");
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void doGetX(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		PrintWriter out = response.getWriter();
		out.println("<html>");
		out.println("<body>");
		out.println("<h1>Hello Servlet Get</h1>");
		out.println("</body>");
		out.println("</html>");
	}
}
